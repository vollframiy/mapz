﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MAPZ_Lab01_Project1
{
    abstract class Figure
    {
        public abstract bool HasCorners
        {
            get;
        }

        protected internal abstract void introduseFigure();
    }

    interface IMeasureable
    {
        double getLength();
        double getArea();
        double getAssemetry();
    }


    class Triangle : Figure , IMeasureable   // Множинне наслідування
    {
        protected double[] sides;
        internal bool isRight;
        private bool hasCorners;

        public Triangle(double[] arrSides , bool right, bool corners)
        {
            sides = arrSides;
            isRight = right;
            hasCorners = corners;
        }

        public override bool HasCorners
        {
            get { return hasCorners; }
        }

        protected internal override void introduseFigure()
        {
            Console.WriteLine("I am triangle");
        }

        // Реалізація функцій явного і неявного приведення типів
        public static implicit operator double(Triangle tr)
        {
            return ((IMeasureable)tr).getArea();
        }

        public static explicit operator bool(Triangle tr)
        {
            return tr.isRight;
        }


        double IMeasureable.getArea()
        {
            return sides[0] * sides[1] / 2;
        }

        public double getLength()
        {
            return sides[0] + sides[1] + sides[2];
        }

        public double getAssemetry()
        {
            return Functions.veryHardFunction(sides);
        }

        public override string ToString()
        {
            return "Triangle";
        }

    }

    class ColorTriangle : Triangle // Перевантаження конструкторів та base this
    {
        public string color = "yellow";
        public static string textAboutTriangle = "Some text in static variable"; // Інціалізація різними способами

        static ColorTriangle()
        {
            Console.WriteLine($"Value of text before constructor: {textAboutTriangle}");
            textAboutTriangle = "Direct is faster";
        }

        public ColorTriangle(double[] arrSides , bool right, bool corners) : base(arrSides, right, corners)
        {
            Console.WriteLine($"Value of color before constructor: {color}");
            this.color = "green";
        }

        public ColorTriangle(double[] arrSides, bool right, bool corners, string color) : this(arrSides, right, corners)
        {
            this.color = color;
        }

        protected internal override void introduseFigure()
        {
            base.introduseFigure();
            Console.WriteLine($"I am triangle that have {color} color");

            //this.sides - достпуний protected
            //this.hasCorners - не доступний private
        }
    }
}



