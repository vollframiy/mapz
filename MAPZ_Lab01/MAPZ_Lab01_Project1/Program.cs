﻿using System;

namespace MAPZ_Lab01_Project1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Boxing - пакування
            Figure anyFigure = new Triangle(new double[3] { 4, 5, 2 }, true, true);

            // Unboxing - розпаковування
            Triangle newTr = (Triangle)anyFigure;
            newTr.introduseFigure();

            //Використання перевизначених функцій з абстрактного класу і реалізація інтерфейсу
            Console.WriteLine($"Length of triangle: {newTr.getLength()}");
            Console.WriteLine($"Area of triangle across interface function: {((IMeasureable)newTr).getArea()}");

            // newTr.hasCorenr - не доступний тут private
            //newTr.sides;// - не доступний тут protectedж
            // newTr.isRight - доступний internal

            //Implict перетворення в double та explict перетворення в bool
            if ((bool)newTr)
            {
                double temp = newTr;
                Console.WriteLine($"Implict changed newTr in double: {temp}");
            }

            //Перевантаження метода класу Object
            Console.WriteLine($"Used override function of Object class: " + newTr.ToString());

            // Перевантаження конструкторів, використання base, this
            ColorTriangle cTr = new ColorTriangle(new double[3] { 4, 5, 2 }, true, true);

            // Без вказування модифікатора доступу по дефолту для класів і структур поля будуть private
            Car car1 = new Car("Ford");
            stCar car2 = new stCar("Ford");

            // Iнтерфейс має по дефолту public поля
            ((IDrive)car1).Drive();

            // Функція повернення внутрішнього класу не повинна мати вищий доступ ніж внутрішній клас
            Family family = new Family(4, car1);
            //family.getDog();

            // Булівські операції з enum
            Days days = Days.Monday | Days.Tuesday | Days.Wednesday | Days.Saturday | Days.Sunday;
            Days weekend = Days.Saturday | Days.Sunday;
            Days saturday = weekend & Days.Saturday;

            if (saturday == Days.Friday || weekend == (days & weekend))
            {
                Console.WriteLine($"Element of enum type: {days}");
            }

            //Використання ref і out
            int val1 = -5, val2 = 5;
            Console.WriteLine("Value after function without ref and after with ref:");

            Functions.NotChangeValue(val1, val2);
            Console.WriteLine($"val1 : {val1} , val2 : {val2}");
            Functions.ChangeValue(ref val1, out val2);
            Console.WriteLine($"val1 : {val1} , val2 : {val2}");
        }

    }
}
