﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace MAPZ_Lab01_Project1
{
    class Functions
    {
        public static double veryHardFunction(double[] sides)
        {
            return sides[0];
        }

        public static double Add(double a1, double a2) //Перевантажені функції
        {
            return a1 + a2;
        }

        public static double Add(double a1, double a2, double a3)
        {
            return a1 + a2 + a3;
        }

        public static void ChangeValue(ref int value, out int value2)
        {
            value2 = 12; // це обов'язково 
            value = 12; // це не обов'язково 
        }

        public static void NotChangeValue(int value, int value2)
        {
            value2 = 12;
            value = 12; 
        }

        public static void NoDifference(int[] arr)
        {
            arr = new int[] { 1, 2, 4 };
        }
    }
}
