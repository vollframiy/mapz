﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MAPZ_Lab01_Project1
{
    interface IDrive // Поля public за замовчуванням
    {
        void Drive();
    }

    struct stCar  // Поля private за замовчуванням
    {
        string name;

        public stCar(string name)
        {
            this.name = name;
        }

        void introduseCar()
        {
            Console.WriteLine($"I am a Car in structure that called {name}");
        }
    }

    class Car : IDrive // Поля private за замовчуванням
    {
        string name = "none";

        public Car(string name)
        {
            this.name = name;
        }

        void introduseCar()
        {
            Console.WriteLine($"I am a Car that called {name}");
        }

        void IDrive.Drive() // public
        {
            Console.WriteLine("I can drive");
        }
    }

    class Family
    {
        Car carOfFamily;
        int countMembers;

        // Під час успадкування не можливо доступитись до батьківського констркутора якщо немає модифікатора доступу
        public Family(int count, Car car)
        {
            this.carOfFamily = car;
            this.countMembers = count;
        }

        void FamilyDrive()
        {
            ((IDrive)carOfFamily).Drive(); // Drive доступний але всі інші поля закриті
        }

        public class Dog // Внутрішній клас теж має модифікатор доступу private і
        {
            public int age = 3;
            internal string name = "Deysi";
            private string color = "White";
        }

        // Не можна вмокристовуати функцію public яка повертає клас з protected
        public Dog getDog() 
        {
            return new Dog();
        }

    }

}
