﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MAPZ_Lab01_Project1
{
    [Flags]
    enum Days
    {
        None = 0,
        Monday = 1,
        Tuesday = 2,
        Wednesday = 4,
        Thursday = 8,
        Friday = 16,
        Saturday = 32,
        Sunday = 64,
    }

}
