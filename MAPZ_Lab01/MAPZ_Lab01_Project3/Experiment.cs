﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MAPZ_Lab01_Project3
{
    internal class Experiment
    {
        public int data1;
        public string data2;
        public double[] arr;

        public Experiment(int data1 = 25, string data2 = "Some text")
        {
            this.data1 = 25;
            this.data2 = "Some text";
            arr = new double[] { 1, 2, 3 };
        }
    }

    struct stExperiment
    {
        public int data1;
        public string data2;
        public double[] arr;

        public stExperiment(int data1 = 25, string data2 = "Some text")
        {
            this.data1 = 25;
            this.data2 = "Some text";
            this.arr = new double[] { 1, 2, 3 };
        }
    }
}
