﻿using System;
using System.Diagnostics;

namespace MAPZ_Lab01_Project3
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = Convert.ToInt32(Console.ReadLine());
            Experiment arrExep;
            stExperiment arrStExep;

            Stopwatch stopwatch = new Stopwatch();

            if (a == 1)
            {
                stopwatch.Start();
                for(int i = 0; i < 10000000; i++)
                {
                    arrExep = new Experiment();
                    arrExep.data1 = i;
                }
                stopwatch.Stop();
            }
            else
            {
                stopwatch.Start();
                for (int i = 0; i < 10000000; i++)
                {
                    arrStExep = new stExperiment();
                    arrStExep.data1 = i;
                }
                stopwatch.Stop();
            }

            TimeSpan ts = stopwatch.Elapsed;
            Console.WriteLine($"Time {ts.TotalMilliseconds}");
        }
    }
}
