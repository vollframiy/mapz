﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MAPZ_Lab01_Project2
{

    interface IMeasureable
    {
        double getLength();
        double getArea();
        double getAssemetry();
    }

    interface IDraw
    {
        void Draw();
    }


    struct Triangle :  IMeasureable, IDraw   // Множинне наслідування для структури
    {
        public double[] sides;
        internal bool isRight;
        private bool hasCorners;

        public Triangle(double[] arrSides , bool right, bool corners)
        {
            sides = arrSides;
            isRight = right;
            hasCorners = corners;
        }


        public static explicit operator bool(Triangle tr)
        {
            return tr.isRight;
        }


        double IMeasureable.getArea()
        {
            return sides[0] * sides[1] / 2;
        }

        void IDraw.Draw()
        {
            Console.WriteLine("I draw triangle");
        }

        public double getLength()
        {
            return sides[0] + sides[1] + sides[2];
        }

        public double getAssemetry()
        {
            return Functions.veryHardFunction(sides);
        }


    }

}



