﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MAPZ_Lab02_Project1
{
    class Departament
    {
        public string Name { get; set; }
        public int ID { get; set; }
    }

    public class Employee
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public int YearsExepiriance { get; set; }
        public int DepartamentID { get; set; }
    }

    public class EmployeeComparer : IComparer<Employee>
    { 
        public int Compare(Employee x, Employee y)
        {
            return x.Age - y.Age;
        }
    }

}
