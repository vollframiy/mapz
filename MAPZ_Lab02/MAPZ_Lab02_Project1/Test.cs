﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace MAPZ_Lab02_Project1
{
    class Test
    {
        private static int count = 1;

        public static void ShowAllRows(List<Departament> departs, List<Employee> empls)
        {
            var temp =
                from d in departs
                select d;

            var temp2 =
                from e in empls
                select e;

            Console.WriteLine(count++ + " Test ShowAllRows\n");
            Console.WriteLine(string.Format("| {0,15} | {1,2} |", "Name", "ID"));
            foreach (var item in temp)
            {
                Console.WriteLine(string.Format("| {0,15} | {1,2} |", item.Name, item.ID));
            }
            Console.WriteLine("\n");
            Console.WriteLine(string.Format("| {0,25} | {1,5} | {2,16} | {3,15} |", "Name", "Age", "YearsExepiriance", "DepartamentID"));
            foreach (var item in temp2)
            {
                Console.WriteLine(string.Format("| {0,25} | {1,5} | {2,16} | {3,15} |", item.Name, item.Age, item.YearsExepiriance, item.DepartamentID));
            }
            Console.WriteLine("\n");
        }

        public static void ShowEmployeesAgeLess25(List<Employee> empls) // Перетворення списка в масив
        {
            var temp2 =
                (from e in empls
                where e.Age < 25
                select e).ToArray();

            Console.WriteLine(count++ + " Test ShowEmployeesAgeLess25\n");
            Console.WriteLine(string.Format("| {0,25} | {1,5} | {2,16} | {3,15} |", "Name", "Age", "YearsExepiriance", "DepartamentID"));
            foreach (var item in temp2)
            {
                Console.WriteLine(string.Format("| {0,25} | {1,5} | {2,16} | {3,15} |", item.Name, item.Age, item.YearsExepiriance, item.DepartamentID));
            }
            Console.WriteLine("\n");
        }

        public static void ShowEmployeesNameLengthLess19(List<Employee> empls) // Викорисатння Where, Select
        {
            var temp2 =
                from e in empls
                where e.Name.Length < 19
                select new { Name = e.Name, ID = e.DepartamentID };

            Console.WriteLine(count++ + " Test ShowEmployeesNameLengthLess19\n");
            Console.WriteLine(string.Format("| {0,25} | {1,16} |", "Name", "DepartamentID"));
            foreach (var item in temp2)
            {
                Console.WriteLine(string.Format("| {0,25} | {1,16} |", item.Name, item.ID));
            }
            Console.WriteLine("\n");
        }

        public static void JoinRows(List<Departament> departs, List<Employee> empls)
        {
            var temp =
                from e in empls
                join d in departs
                on e.DepartamentID equals d.ID
                select new { Name = e.Name, DepartamentName = d.Name };  // Використання анонімних класів


            Console.WriteLine(count++ + " Test JoinRows\n");
            Console.WriteLine(string.Format("| {0,25} | {1,25} |", "Name", "DepartamentName"));
            foreach (var item in temp)
            {
                Console.WriteLine(string.Format("| {0,25} | {1,25} |", item.Name, item.DepartamentName));
            }
            Console.WriteLine("\n");
        }

        public static void ShowEmployeesWhere(Dictionary<int, Employee> empls) // Перетворення в список
        {
            var temp2 =
                (from e in empls
                where e.Value.Age > 25 && e.Value.YearsExepiriance > 3
                select e).ToList();

            Console.WriteLine(count++ + " Test ShowEmployeesWhere age > 25 and exper. > 3 \n");
            Console.WriteLine(string.Format("| {0,25} | {1,5} | {2,16} |", "Name", "Age", "YearsExepiriance"));
            foreach (var item in temp2)
            {
                Console.WriteLine(string.Format("| {0,25} | {1,5} | {2,16} |", item.Value.Name, item.Value.Age, item.Value.YearsExepiriance));
            }
            Console.WriteLine("\n");
        }

        public static void ShowEmployeesSortByName(List<Employee> empls) // Сортування
        {
            var temp2 =
                from e in empls
                orderby e.Name
                select new { Name = e.Name, ID = e.DepartamentID };

            Console.WriteLine(count++ + " Test ShowEmployeesSortByName\n");
            Console.WriteLine(string.Format("| {0,25} | {1,16} |", "Name", "DepartamentID"));
            foreach (var item in temp2)
            {
                Console.WriteLine(string.Format("| {0,25} | {1,16} |", item.Name, item.ID));
            }
            Console.WriteLine("\n");
        }
    }
}
