﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;

namespace MAPZ_Lab02_Project1
{
    class Program
    {
        static void Main(string[] args)
        {
            List <Departament> departs = ModelingData.getSomeDepartaments();
            List<Employee> empls = ModelingData.getSomeEmployees();

            Test.ShowAllRows(departs, empls);   // Використання LINQ
            Test.JoinRows(departs, empls);
            Test.ShowEmployeesNameLengthLess19(empls);
            Test.ShowEmployeesSortByName(empls);


            Dictionary<int, Employee> dicEmpls = ModelingData.getSomeEmployees(1); //Використання словника
            Test.ShowEmployeesWhere(dicEmpls);

            double averageOfYears = empls.AvereageYear(); // власний метод розширення
            Console.WriteLine("Custom extemtions methods returned : " + averageOfYears + "\n");

            Console.WriteLine("Sorted list by age by IComparer\n"); // Сортування через шаблон IComparer
            empls.Sort(new EmployeeComparer());
            empls.ForEach(empl => Console.WriteLine(string.Format("| {0,25} | {1,5} |", empl.Name, empl.Age)));


            Dictionary<int, List<Employee>> dicGroup = new Dictionary<int, List<Employee>>(); // Стоврення словника згідно з прикладом
            foreach(var item in departs)
            {
                var list =
                    (from e in empls
                     where e.DepartamentID == item.ID
                     select e).ToList();

                dicGroup.Add(item.ID, list);
            }
        }
    }
}
