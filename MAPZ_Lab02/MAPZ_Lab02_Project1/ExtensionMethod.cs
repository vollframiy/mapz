﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MAPZ_Lab02_Project1
{
    public static class ExtensionMethod
    {
        public static double AvereageYear(this List<Employee> empls)
        {
            double avr = 0;
            int leng = 0;

            foreach(var item in empls)
            {
                avr += item.Age;
                leng++;
            }

            return avr / leng;
        }
    }
}
