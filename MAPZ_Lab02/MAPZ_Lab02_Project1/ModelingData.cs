﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MAPZ_Lab02_Project1
{
    class ModelingData
    {
        public static List<Departament> getSomeDepartaments() // Скрізь використовую ініціалізатори
        {
            List<Departament> departs = new List<Departament>();

            departs.Add(new Departament { Name = "Sales", ID = 1 });
            departs.Add(new Departament { Name = "Development", ID = 2 });
            departs.Add(new Departament { Name = "Analysises", ID = 3 });
            departs.Add(new Departament { Name = "HR`s", ID = 4 });
            departs.Add(new Departament { Name = "Arcihtectors", ID = 5 });

            return departs;
        }

        public static List<Employee> getSomeEmployees() // Скрізь використовую ініціалізатори
        {
            List<Employee> empls = new List<Employee>();

            empls.Add(new Employee { Name = "Vlad Hladkyi", Age = 19, YearsExepiriance = 1, DepartamentID = 2 });
            empls.Add(new Employee { Name = "Mukola Chornuy", Age = 23, YearsExepiriance = 5, DepartamentID = 2 });
            empls.Add(new Employee { Name = "Alina Bob", Age = 25, YearsExepiriance = 2, DepartamentID = 2 });
            empls.Add(new Employee { Name = "Oleg Boyda", Age = 21, YearsExepiriance = 3, DepartamentID = 1 });
            empls.Add(new Employee { Name = "Olga Pignovska", Age = 22, YearsExepiriance = 3, DepartamentID = 1 });
            empls.Add(new Employee { Name = "Ivan Dyakonyak", Age = 19, YearsExepiriance = 1, DepartamentID = 1 });
            empls.Add(new Employee { Name = "Mukola Andryzelyak", Age = 26, YearsExepiriance = 4, DepartamentID = 1 });
            empls.Add(new Employee { Name = "Maria Glexner", Age = 24, YearsExepiriance = 2, DepartamentID = 4 });
            empls.Add(new Employee { Name = "Pavlo Skorohodko", Age = 29, YearsExepiriance = 6, DepartamentID = 4 });
            empls.Add(new Employee { Name = "Anastasia Dragometskya", Age = 24, YearsExepiriance = 5, DepartamentID = 5 });
            empls.Add(new Employee { Name = "Vlad Boyko", Age = 20, YearsExepiriance = 1, DepartamentID = 3 });
            empls.Add(new Employee { Name = "Alina Zachybayko", Age = 32, YearsExepiriance = 2, DepartamentID = 3 });
            empls.Add(new Employee { Name = "Ivan Hladkyi", Age = 31, YearsExepiriance = 2, DepartamentID = 3 });
            empls.Add(new Employee { Name = "Mukola Vdovin", Age = 27, YearsExepiriance = 3, DepartamentID = 2 });
            empls.Add(new Employee { Name = "Igor Brenutskiy", Age = 34, YearsExepiriance = 4, DepartamentID = 1 });

            return empls;
        }

        public static Dictionary<int, Employee> getSomeEmployees(int flag) // Скрізь використовую ініціалізатори
        {
            Dictionary<int, Employee> empls = new Dictionary<int, Employee>();

            empls.Add(0, new Employee { Name = "Vlad Hladkyi", Age = 19, YearsExepiriance = 1, DepartamentID = 2 });
            empls.Add(1, new Employee { Name = "Mukola Chornuy", Age = 23, YearsExepiriance = 5, DepartamentID = 2 });
            empls.Add(2, new Employee { Name = "Alina Bob", Age = 25, YearsExepiriance = 2, DepartamentID = 2 });
            empls.Add(3, new Employee { Name = "Oleg Boyda", Age = 21, YearsExepiriance = 3, DepartamentID = 1 });
            empls.Add(4, new Employee { Name = "Olga Pignovska", Age = 22, YearsExepiriance = 3, DepartamentID = 1 });
            empls.Add(5, new Employee { Name = "Ivan Dyakonyak", Age = 19, YearsExepiriance = 1, DepartamentID = 1 });
            empls.Add(6, new Employee { Name = "Mukola Andryzelyak", Age = 26, YearsExepiriance = 4, DepartamentID = 1 });
            empls.Add(7, new Employee { Name = "Maria Glexner", Age = 24, YearsExepiriance = 2, DepartamentID = 4 });
            empls.Add(8, new Employee { Name = "Pavlo Skorohodko", Age = 29, YearsExepiriance = 6, DepartamentID = 4 });
            empls.Add(9, new Employee { Name = "Anastasia Dragometskya", Age = 24, YearsExepiriance = 5, DepartamentID = 5 });
            empls.Add(10, new Employee { Name = "Vlad Boyko", Age = 20, YearsExepiriance = 1, DepartamentID = 3 });
            empls.Add(11, new Employee { Name = "Alina Zachybayko", Age = 32, YearsExepiriance = 2, DepartamentID = 3 });
            empls.Add(12, new Employee { Name = "Ivan Hladkyi", Age = 31, YearsExepiriance = 2, DepartamentID = 3 });
            empls.Add(13, new Employee { Name = "Mukola Vdovin", Age = 27, YearsExepiriance = 3, DepartamentID = 2 });
            empls.Add(14, new Employee { Name = "Igor Brenutskiy", Age = 34, YearsExepiriance = 4, DepartamentID = 1 });

            return empls;
        }
    }
}
